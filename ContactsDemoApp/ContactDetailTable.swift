//
//  ContactDetailTable.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

enum ContactRowType: String, EnumStringRepresentable {
    case Username
    case Phone
    case Address
    case Website
    case Company
}

extension ContactDetailViewController: UITableViewDelegate {

    func reloadContent() {
        rows = [ContactRowType]()

        if model.username != nil {
            rows.append(.Username)
        }

        if model.phone != nil {
            rows.append(.Phone)
        }

        if model.address != nil {
            rows.append(.Address)
        }

        if model.website != nil {
            rows.append(.Website)
        }

        if model.company != nil {
            rows.append(.Company)
        }

        tableView.reloadData()
    }

    // MARK: UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(ContactCell.Identifier) as! ContactCell

        cell.titleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
        cell.descriptionLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)

        let rowType = rows[indexPath.row]

        switch rowType {
        case .Username:
            cell.titleLabel.text = model.username
            cell.descriptionLabel.text = NSLocalizedString("contact-detail-username", value: "Username", comment: "Title - contact's username").uppercaseString

        case .Phone:
            cell.titleLabel.text = model.phone
            cell.descriptionLabel.text = NSLocalizedString("contact-detail-phone", value: "Phone", comment: "Title - contact's phone").uppercaseString

        case .Address:
            cell.titleLabel.text = model.address
            cell.descriptionLabel.text = NSLocalizedString("contact-detail-address", value: "Address", comment: "Title - contact's address").uppercaseString

        case .Website:
            cell.titleLabel.text = model.website
            cell.descriptionLabel.text = NSLocalizedString("contact-detail-website", value: "Website", comment: "Title - contact's website").uppercaseString

        case .Company:
            cell.titleLabel.text = model.company
            cell.descriptionLabel.text = NSLocalizedString("contact-detail-company", value: "Company", comment: "Title - contact's company").uppercaseString
        }

        return cell
    }
}
