//
//  Contact.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

struct Contact {

    let id: UInt
    private(set) var name: String?
    private(set) var username: String?
    private(set) var phone: String?
    private(set) var email: String?
    private(set) var website: String?

    private(set) var address: ContactAddress?
    private(set) var company: ContactCompany?

    init?(dictionary: [String: AnyObject?]) {
        guard let id = dictionary["id"] as? UInt else {
            print("Contact is missing identifier \(dictionary)")
            return nil
        }

        self.id = id
        name = dictionary["name"] as? String
        username = dictionary["username"] as? String
        phone = dictionary["phone"] as? String
        email = dictionary["email"] as? String
        website = dictionary["website"] as? String

        if let dic = dictionary["address"] as? [String : AnyObject] {
            address = ContactAddress(dictionary: dic)
        }

        if let dic = dictionary["company"] as? [String : AnyObject] {
            company = ContactCompany(dictionary: dic)
        }
    }
}