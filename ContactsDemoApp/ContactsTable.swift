//
//  ContactsTable.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {

    // MARK: UITableViewDataSource

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.listModels.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContactCell") as! ContactCell

        let contact = model.listModels[indexPath.row]

        cell.titleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        cell.descriptionLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)

        cell.titleLabel.text = contact.name
        cell.descriptionLabel.text = contact.email

        return cell
    }

    // MARK: UITableViewDelegate

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let contact = model.listModels[indexPath.row]

        if !model.presentContactDetail(contact) {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
}