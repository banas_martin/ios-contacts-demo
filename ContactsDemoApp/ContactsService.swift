//
//  ContactsService.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation
import Result

class ContactsService {

    private let networking: URLSessionProtocol
    private let requestTimeout: NSTimeInterval = 30
    private let configuration: AppConfiguration

    init(networking: URLSessionProtocol, configuration: AppConfiguration) {
        self.networking = networking
        self.configuration = configuration
    }

    // MARK: Public methods

    /// Completion is fired on arbitrary thread
    func fetchContacts(completion: ((Result<[Contact]?, NSError>) -> Void)) {

        let request = NSMutableURLRequest(URL: configuration.contactListURL, cachePolicy: .ReturnCacheDataElseLoad, timeoutInterval: requestTimeout)
        request.HTTPMethod = "GET"

        networking.dataTaskWithRequest(request) { [weak self](data, response, error) in
            guard let ws = self else {
                return
            }

            if let error = error {
                completion(.Failure(error))
                return
            }

            guard let data = data else {
                let reason = NSLocalizedString("contacts-service-data-failure", value: "Missing contacts data", comment: "Message mostly used for presenting alert's dialog - service is missing input data to parse")
                completion(.Failure(Error.error(code: -1, failureReason: reason)))
                return
            }

            ws.processContactsData(data, completion: completion)

        }.resume()
    }

    // MARK: Private methods

    private func processContactsData(data: NSData, completion: ((Result<[Contact]?, NSError>) -> Void)) {
        var contacts = [Contact]()

        do {
            let list = try NSJSONSerialization.JSONObjectWithData(data, options: [])

            guard let contactList = list as? [[String: AnyObject]] else {
                let reason = NSLocalizedString("contacts-service-json-failure", value: "Unable to parse contacts from json", comment: "Message mostly used for presenting alert's dialog - service is not able to parse contacts from json")
                completion(.Failure(Error.error(code: -1, failureReason: reason)))
                return
            }

            for dic in contactList {
                if let contact = Contact(dictionary: dic) {
                    contacts.append(contact)
                }
            }

            completion(.Success(contacts))

        } catch let error as NSError {
            completion(.Failure(error))
        }
    }
}
