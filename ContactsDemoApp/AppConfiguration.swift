//
//  AppConfiguration.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

struct AppConfiguration {

    /// Live contact list url
    var contactListURL: NSURL {
        return NSURL(string: "http://jsonplaceholder.typicode.com/users")!
    }
}
