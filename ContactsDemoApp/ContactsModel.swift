//
//  ContactsModel.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class ContactsModel {

    var reloadHandler: (() -> Void)?
    var errorHandler: ((message: String) -> Void)?
    var ordered: NSComparisonResult = .OrderedAscending

    let title: String

    private let fetchResult: ContactsFetchResult
    private(set) var listModels  = [ContactListModel]()
    private weak var coordinator: ContactsCoordinator?

    init(coordinator: ContactsCoordinator, fetchResult: ContactsFetchResult) {
        self.coordinator = coordinator
        self.fetchResult = fetchResult
        self.title = NSLocalizedString("contact-list-navigation-title", value: "Contacts", comment: "Navigation bar title")
    }

    // MARK: Public methods

    func flipSort() {
        ordered = (ordered == .OrderedAscending) ? .OrderedDescending : .OrderedAscending

        if let contacts = fetchResult.contacts {
            processFetchedContacts(contacts)
        }
    }

    func fetchContacts() {
        if let error = fetchResult.error {
            let message = (error.userInfo[NSLocalizedFailureReasonErrorKey] as? String) ?? NSLocalizedString("generic-alert-message", value: "Sorry, something went wrong", comment: "Generis alert's dialog message - when something goes wrong")
            errorHandler?(message: message)

        } else if let contacts = fetchResult.contacts {
            processFetchedContacts(contacts)
        }
    }

    // MARK: Coordinator

    func presentContactDetail(listModel: ContactListModel) -> Bool {
        if let contact = fetchResult.contacts?.filter({ $0.id == listModel.id }).first {
            coordinator?.presentContactDetail(contact)
            return true
            
        } else {
            return false
        }
    }

    func refresh() {
        coordinator?.finished()
    }

    // MARK: Private methods 

    private func processFetchedContacts(contacts: [Contact]) {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) { [weak self] in
            guard let ws = self else {
                return
            }

            let models: [ContactListModel] = contacts.map({
                return ContactListModel(contact: $0)

            }).sort { (first, second) -> Bool in
                return first.name.localizedStandardCompare(second.name) == ws.ordered
            }

            dispatch_async(dispatch_get_main_queue()) {
                ws.listModels = models
                ws.reloadHandler?()
            }
        }
    }
}
