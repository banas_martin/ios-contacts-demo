//
//  AppCoordinator.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit
import Result

final class AppCoordinator: Coordinator {

    var identifier: String {
        return "app"
    }

    weak var parent: Coordinator?

    var rootViewController: UIViewController {
        return containerController
    }

    let window: UIWindow

    private var containerController: LaunchViewController!
    private var coordinators = [Coordinator]()
    private let contactsService: ContactsService

    init() {
        let window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window = window

        containerController = LaunchViewController()

        let networking = NSURLSession.sharedSession()
        let configuration = AppConfiguration()

        contactsService = ContactsService(networking: networking, configuration: configuration)
    }

    func start() {
        print("starting \(identifier) flow")

        containerController.initialized = { [weak self]() in
            self?.initial(false)

        }

        window.makeKeyAndVisible()
        window.rootViewController = containerController
    }

    func finish(coordinator: Coordinator) {
        removeCoordinator(coordinator)
        
        // User has finished contacts flow
        if coordinator is ContactsCoordinator {
            initial(true)
        }
    }

    // MARK: Private methods

    private func initial(animated: Bool) {
        coordinators.removeAll()

        presentLoading(animated)

        /// Completion is fired on arbitrary thread
        contactsService.fetchContacts { [weak self](result) in
            let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))

            dispatch_after(dispatchTime, dispatch_get_main_queue(), { _ in
                self?.presentContacts(ContactsFetchResult(result: result))
            })
        }
    }

    private func presentLoading(animated: Bool) {
        let controller = LoadingViewController()

        let animator = BaseAnimatedTransition(duration: 0.25)
        containerController.presentViewController(controller, animated: animated, animator: animated ? animator : nil, completion: nil)
    }

    private func presentContacts(fetchResult: ContactsFetchResult) {

        let coordinator = ContactsCoordinator(parent: self, fetchResult: fetchResult)
        coordinators.append(coordinator)

        let animator = BaseAnimatedTransition(duration: 0.25)
        containerController.presentViewController(coordinator.rootViewController, animated: true, animator: animator, completion: nil)
    }

    private func removeCoordinator(coordinator: Coordinator) {
        if let index = coordinators.indexOf({ (c) -> Bool in c === coordinator }) {
            coordinators.removeAtIndex(index)
        }
    }
}
