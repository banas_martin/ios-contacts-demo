//
//  EnumStringRepresentable.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

protocol EnumStringRepresentable {

    var rawValue: String { get }
}