//
//  ContactDetailModel.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class ContactDetailModel: NSObject {

    let title: String
    let username: String?
    let phone: String?
    let address: String?
    let website: String?
    let company: String?

    private weak var coordinator: Coordinator?

    init(coordinator: ContactsCoordinator, contact: Contact) {
        self.coordinator = coordinator

        let title = contact.name ?? NSLocalizedString("contact-detail-navigation-title-generic", value: "Detail", comment: "Navigation bar title")
        self.title = title.uppercaseString

        username = contact.username
        phone = contact.phone
        website = contact.website
        address = contact.address?.formatted
        company = contact.company?.formatted
    }
}
