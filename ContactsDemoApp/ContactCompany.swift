//
//  ContactCompany.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

struct ContactCompany {

    private(set) var name: String?
    private(set) var catchPhrase: String?
    private(set) var bs: String?

    init(dictionary: [String: AnyObject?]) {
        name = dictionary["name"] as? String
        catchPhrase = dictionary["catchPhrase"] as? String
        bs = dictionary["bs"] as? String
    }
}

extension ContactCompany {

    var formatted: String? {
        var rows = [String]()

        if let s = name where s.isEmpty == false {
            rows.append(s)
        }

        if let s = catchPhrase where s.isEmpty == false {
            rows.append(s)
        }

        if let s = bs where s.isEmpty == false {
            rows.append(s)
        }

        return rows.isEmpty ? nil : rows.joinWithSeparator("\n")
    }
}
