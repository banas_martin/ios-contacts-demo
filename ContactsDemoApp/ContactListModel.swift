//
//  ContactListModel.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

struct ContactListModel {

    let id: UInt
    let name: String
    let email: String

    init(contact: Contact) {
        id = contact.id
        name = contact.name ?? ""
        email = contact.email ?? ""
    }
}
