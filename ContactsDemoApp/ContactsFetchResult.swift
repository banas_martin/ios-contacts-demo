//
//  ContactsFetchResult.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation
import Result

struct ContactsFetchResult {

    private(set) var contacts: [Contact]?
    private(set) var error: NSError?

    init(result: Result<[Contact]?, NSError>) {

        switch result {
        case .Success(let contacts):
            self.contacts = contacts

        case .Failure(let error):
            self.error = error
        }
    }
}
