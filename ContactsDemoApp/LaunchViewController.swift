//
//  LaunchViewController.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class LaunchViewController: ContainerViewController {

    var initialized: (() -> Void)?

    private var dispatchOncePredicate: dispatch_once_t = 0

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        dispatch_once(&dispatchOncePredicate, { [weak self]() -> Void in
            self?.initialized?()
        })
    }
}