//
//  ContactAddress.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation
import CoreLocation.CLLocation

struct ContactAddress {

    private(set) var street: String?
    private(set) var suite: String?
    private(set) var city: String?
    private(set) var zipcode: String?
    private(set) var longtitude: String?
    private(set) var latitude: String?

    init(dictionary: [String: AnyObject?]) {
        street = dictionary["street"] as? String
        suite = dictionary["suite"] as? String
        city = dictionary["city"] as? String
        zipcode = dictionary["zipcode"] as? String

        if let geo = dictionary["geo"] as? [String: String] {
            latitude = geo["lat"]
            longtitude = geo["lng"]
        }
    }
}

extension ContactAddress {

    var formatted: String? {
        var rows = [String]()

        if let s = street where s.isEmpty == false {
            rows.append(s)
        }

        if let s = suite where s.isEmpty == false {
            rows.append(s)
        }

        if let s = city where s.isEmpty == false {
            if let sub = zipcode where s.isEmpty == false {
                rows.append(s + ", " + sub)

            } else {
                rows.append(s)
            }

        } else if let s = zipcode where s.isEmpty == false {
            rows.append(s)
        }

        return rows.isEmpty ? nil : rows.joinWithSeparator("\n")
    }
}
