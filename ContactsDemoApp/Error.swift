//
//  Error.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation

// Custom error factory
struct Error {

    static let Domain = "com.contatcts-demo-app.error"

    static func error(domain domain: String = Error.Domain, code: Int, failureReason: String) -> NSError {
        return errorWithCode(code, failureReason: failureReason)
    }

    private static func errorWithCode(code: Int, failureReason: String) -> NSError {
        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
        return NSError(domain: Domain, code: code, userInfo: userInfo)
    }
}