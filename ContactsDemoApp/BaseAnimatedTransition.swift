//
//  BaseAnimatedTransition.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

final class BaseAnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning {

    private let duration: NSTimeInterval

    init(duration: NSTimeInterval) {
        self.duration = duration
        super.init()
    }

    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return transitionContext?.isAnimated() == true ? duration : 0
    }

    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!

        transitionContext.containerView()?.addSubview(toViewController.view)
        toViewController.view.alpha = 0
        toViewController.view.layoutIfNeeded()

        if transitionContext.isAnimated() {
            UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
                toViewController.view.alpha = 1
                
            }) { (done) -> Void in
                toViewController.view.alpha = 1
                fromViewController.view.alpha = 0
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            }

        } else {
            toViewController.view.alpha = 1
            fromViewController.view.alpha = 0
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
    }
}