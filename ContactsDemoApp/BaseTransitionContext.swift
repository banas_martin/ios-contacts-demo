//
//  BaseTransitionContext.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

final class BaseTransitionContext: NSObject, UIViewControllerContextTransitioning {

    var completion: ((didComplete: Bool) -> Void)?
    var animated = true
    var interactive = false

    private let viewControllers: [String: UIViewController]
    private let views: [String: UIView]
    private weak var privateContainerView: UIView?

    private let disappearingFromRect: CGRect
    private let appearingFromRect: CGRect
    private let disappearingToRect: CGRect
    private let appearingToRect: CGRect

    init(fromViewController: UIViewController, toViewController: UIViewController) {
        viewControllers = [UITransitionContextFromViewControllerKey: fromViewController, UITransitionContextToViewControllerKey: toViewController]
        views = [UITransitionContextFromViewKey: fromViewController.view, UITransitionContextToViewKey: toViewController.view]

        privateContainerView = fromViewController.view?.superview

        let travelDistance = privateContainerView!.bounds.size.width
        disappearingFromRect = privateContainerView!.bounds
        disappearingToRect = CGRectOffset(privateContainerView!.bounds, travelDistance, 0)
        appearingFromRect = CGRectOffset(privateContainerView!.bounds, -travelDistance, 0)
        appearingToRect = privateContainerView!.bounds

        super.init()
    }

    // MARK: UIViewControllerContextTransitioning methods

    func containerView() -> UIView? {
        return privateContainerView
    }

    func initialFrameForViewController(vc: UIViewController) -> CGRect {
        if let _ = viewControllerForKey(UITransitionContextFromViewControllerKey) {
            return disappearingFromRect
        } else {
            return appearingFromRect
        }
    }

    func finalFrameForViewController(vc: UIViewController) -> CGRect {
        if let _ = viewControllerForKey(UITransitionContextFromViewControllerKey) {
            return disappearingToRect
        } else {
            return appearingToRect
        }
    }

    func viewControllerForKey(key: String) -> UIViewController? {
        return viewControllers[key]
    }

    func viewForKey(key: String) -> UIView? {
        return views[key]
    }

    func completeTransition(didComplete: Bool) {
        completion?(didComplete: didComplete)
    }

    func targetTransform() -> CGAffineTransform {
        return CGAffineTransformIdentity
    }

    func isAnimated() -> Bool {
        return animated
    }

    func isInteractive() -> Bool {
        return interactive
    }

    func presentationStyle() -> UIModalPresentationStyle {
        return .Custom
    }

    func transitionWasCancelled() -> Bool {
        return false
    }

    func updateInteractiveTransition(percentComplete: CGFloat) {
        /// Interactive transition is not implemented
    }

    func finishInteractiveTransition() {
        /// Interactive transition is not implemented
    }
    
    func cancelInteractiveTransition() {
        /// Interactive transition is not implemented
    }
}