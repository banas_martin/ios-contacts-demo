//
//  Coordinator.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

protocol Coordinator: class {

    var identifier: String { get }

    var rootViewController: UIViewController { get }

    weak var parent: Coordinator? { get }

    func finish(coordinator: Coordinator)
}