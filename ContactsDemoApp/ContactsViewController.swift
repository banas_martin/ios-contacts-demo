//
//  ContactsViewController.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

extension ContactsViewController {

    class func create(model: ContactsModel) -> ContactsViewController {
        let controller = UIStoryboard(name: "Contacts", bundle: NSBundle(forClass: ContactsViewController.self)).instantiateViewControllerWithIdentifier("ContactsViewController") as! ContactsViewController
        controller.model = model
        return controller
    }
}

class ContactsViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!

    private(set) var model: ContactsModel!
    private var firstLoad = true

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = model.title

        tableView.estimatedRowHeight = 54
        tableView.rowHeight = UITableViewAutomaticDimension

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(refresh))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: #selector(flipSort))

        model.reloadHandler = { [weak self] in
            self?.reloadContent()
        }

        model.errorHandler = { [weak self] (message) in
            self?.presentMessage(message)
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if firstLoad {
            firstLoad = false
            model.fetchContacts()

        } else {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                tableView.deselectRowAtIndexPath(selectedIndexPath, animated: false)

            } else {
                reloadContent()
            }
        }

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dynamicTypeChange(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }

    // MARK: Private methods

    private func reloadContent() {
        tableView.reloadData()
    }

    private func presentMessage(message: String) {
        let alert = UIAlertController(title: NSLocalizedString("generic-alert-title", value: "Error", comment: "Alert's dialog title - when something goes wrong"), message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("generic-alert-cancel", value: "Dismiss", comment: "Alert's dialog close button - closes the alert dialog"), style: .Cancel, handler: nil)
        alert.addAction(cancelAction)

        presentViewController(alert, animated: true, completion: nil)
    }

    @objc private func refresh() {
        model.refresh()
    }

    @objc private func flipSort() {
        model.flipSort()
    }

    // MARK: Dynamic Type

    @objc private func dynamicTypeChange(notification: NSNotification) {
        tableView.reloadData()
    }
}
