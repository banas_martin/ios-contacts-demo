//
//  ContactsCoordinator.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

final class ContactsCoordinator: Coordinator {

    var identifier: String {
        return "contacts"
    }

    weak var parent: Coordinator?

    var rootViewController: UIViewController {
        return navigationController
    }

    private var navigationController: UINavigationController!
    private var viewController: ContactsViewController!

    init(parent: Coordinator, fetchResult: ContactsFetchResult) {
        self.parent = parent
        
        let model = ContactsModel(coordinator: self, fetchResult: fetchResult)
        viewController = ContactsViewController.create(model)

        navigationController = UINavigationController(rootViewController: viewController)

        print("starting \(identifier) flow")
    }

    func finish(coordinator: Coordinator) {
        /// Not implemented
    }

    func presentContactDetail(contact: Contact) {
        let model = ContactDetailModel(coordinator: self, contact: contact)
        let controller = ContactDetailViewController.create(model)

        navigationController.pushViewController(controller, animated: true)
    }

    func finished() {
        print("finishing \(identifier) flow")
        parent?.finish(self)
    }
}
