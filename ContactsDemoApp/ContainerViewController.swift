//
//  ContainerViewController.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    var selectedViewController: UIViewController?
    var containerView: UIView!

    private var transitioning = false

    // MARK: View Controller Life Cycle

    init() {
        super.init(nibName: nil, bundle: nil)
        initializer()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializer()
    }

    override func loadView() {
        super.loadView()
        setupViews()
    }

    func initializer() {
        automaticallyAdjustsScrollViewInsets = false
        extendedLayoutIncludesOpaqueBars = true
        edgesForExtendedLayout = .All
    }

    func setupViews() -> UIView {
        let rootView = view
        rootView.backgroundColor = UIColor.blackColor()
        rootView.opaque = true

        containerView = UIView()
        containerView.backgroundColor = UIColor.clearColor()
        containerView.opaque = true
        containerView.translatesAutoresizingMaskIntoConstraints = false

        rootView.addSubview(containerView)

        rootView.addConstraint(NSLayoutConstraint(item: containerView, attribute: .Width, relatedBy: .Equal, toItem: rootView, attribute: .Width, multiplier: 1, constant: 0))
        rootView.addConstraint(NSLayoutConstraint(item: containerView, attribute: .Height, relatedBy: .Equal, toItem: rootView, attribute: .Height, multiplier: 1, constant: 0))
        rootView.addConstraint(NSLayoutConstraint(item: containerView, attribute: .Left, relatedBy: .Equal, toItem: rootView, attribute: .Left, multiplier: 1, constant: 0))
        rootView.addConstraint(NSLayoutConstraint(item: containerView, attribute: .Top, relatedBy: .Equal, toItem: rootView, attribute: .Top, multiplier: 1, constant: 0))

        return rootView
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return selectedViewController?.preferredStatusBarStyle() ?? .LightContent
    }

    // MARK: Public methods

    func presentViewController(toViewController: UIViewController, animated: Bool, animator: UIViewControllerAnimatedTransitioning?, completion: ((didComplete: Bool) -> Void)?) {
        transitToChildViewController(toViewController, animated: animated, animator: animator) { [weak self] (didComplete) -> Void in
            if didComplete {
                self?.selectedViewController = toViewController
                self?.setNeedsStatusBarAppearanceUpdate()
            }

            completion?(didComplete: didComplete)
        }
    }

    func transitToChildViewController(toViewController: UIViewController, animated: Bool, animator: UIViewControllerAnimatedTransitioning?, completion: ((didComplete: Bool) -> Void)?) {
        if transitioning {
            completion?(didComplete: false)
            return
        }

        let fromViewController = selectedViewController

        if fromViewController == toViewController || !isViewLoaded() {
            completion?(didComplete: false)
            return
        }

        transitioning = true

        let toView = toViewController.view
        toView.translatesAutoresizingMaskIntoConstraints = true
        toView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        toView.frame = containerView.bounds
        toView.layoutIfNeeded()

        selectedViewController?.willMoveToParentViewController(nil)
        addChildViewController(toViewController)

        if fromViewController == nil {
            containerView.addSubview(toViewController.view)
            toViewController.didMoveToParentViewController(self)

            transitioning = false
            completion?(didComplete: true)

        } else {
            fromViewController?.beginAppearanceTransition(false, animated: animated)
            toViewController.beginAppearanceTransition(true, animated: animated)

            UIView.performWithoutAnimation({ () -> Void in
                fromViewController!.view.endEditing(true)
                fromViewController!.dismissViewControllerAnimated(false, completion: nil)
            })

            let transitionAnimator = animator ?? BaseAnimatedTransition(duration: 0.25)
            let transitionContext = BaseTransitionContext(fromViewController: fromViewController!, toViewController: toViewController)

            transitionContext.animated = animated
            transitionContext.interactive = false

            transitionContext.completion = { (didComplete) in
                if didComplete {
                    fromViewController!.view.removeFromSuperview()
                    fromViewController!.removeFromParentViewController()
                    toViewController.didMoveToParentViewController(self)

                    fromViewController?.endAppearanceTransition()
                    toViewController.endAppearanceTransition()

                } else {
                    toViewController.beginAppearanceTransition(false, animated: animated)
                    toViewController.view.removeFromSuperview()
                    toViewController.removeFromParentViewController()
                    toViewController.endAppearanceTransition()

                    fromViewController!.beginAppearanceTransition(true, animated: animated)
                    fromViewController!.didMoveToParentViewController(self)
                    fromViewController!.endAppearanceTransition()
                }

                self.view.userInteractionEnabled = true
                transitionAnimator.animationEnded?(didComplete)
                
                self.transitioning = false
                completion?(didComplete: didComplete)
            }
            
            view.userInteractionEnabled = false
            transitionAnimator.animateTransition(transitionContext)
        }
    }
}