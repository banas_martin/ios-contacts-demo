//
//  ContactDetailViewController.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import UIKit

extension ContactDetailViewController {

    class func create(model: ContactDetailModel) -> ContactDetailViewController {
        let controller = UIStoryboard(name: "ContactDetail", bundle: NSBundle(forClass: ContactDetailViewController.self)).instantiateViewControllerWithIdentifier("ContactDetailViewController") as! ContactDetailViewController
        controller.model = model
        return controller
    }
}

class ContactDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var rows = [ContactRowType]()

    private(set) var model: ContactDetailModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = model.title

        tableView.estimatedRowHeight = 54
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorColor = UIColor.clearColor()

        reloadContent()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dynamicTypeChange(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }

    // MARK: Dynamic Type

    @objc private func dynamicTypeChange(notification: NSNotification) {
        tableView.reloadData()
    }
}
