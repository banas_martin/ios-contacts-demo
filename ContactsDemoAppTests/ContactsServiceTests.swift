//
//  ContactsDemoAppTests.swift
//  ContactsDemoAppTests
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import XCTest
@testable import ContactsDemoApp

class ContactsServiceTests: XCTestCase {

    var config = AppConfiguration()
    var networking: URLSessionMock!
    var contactsService: ContactsService!

    override func setUp() {
        super.setUp()

        config = AppConfiguration()
        networking = URLSessionMock()

        contactsService = ContactsService(networking: networking, configuration: config)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test__receive_contact_list() {

        // Prepare
        let url = config.contactListURL
        let response = TaskResponseMock(requestURL: url, delay: 1)

        /// In tests we don't care about explicit unwrapping
        let path = NSBundle(forClass: self.dynamicType).pathForResource("Contacts", ofType: "plist")!
        let list = NSArray(contentsOfFile: path) as! [[String: AnyObject]]
        let data = try! NSJSONSerialization.dataWithJSONObject(list, options: [])

        response.data = data
        networking.reponseMocks.append(response)

        var originContacts = [Contact]()
        for dic in list {
            if let contact = Contact(dictionary: dic) {
                originContacts.append(contact)
            }
        }

        let expectations = expectationWithDescription("expectations")

        // Test
        contactsService.fetchContacts { (result) in

            // Verify
            switch result {
            case .Success(let contacts):

                XCTAssertNotNil(contacts, "contact list shout not by empty")

                let lastOrigin = originContacts.last!
                let lastFetched = contacts!.last!

                XCTAssertTrue(lastOrigin.name == lastFetched.name, "doesn't contain same contact list")

            case .Failure(let error):
                XCTFail("completed with error \(error)")
            }

            expectations.fulfill()
        }

        waitForExpectationsWithTimeout(10) { (error: NSError?) -> Void in
            guard error == nil else {
                XCTFail("The request took too long to be processed")
                return
            }
        }
    }

    func test__receive_contact_list_error() {

        // Prepare
        let url = config.contactListURL
        let response = TaskResponseMock(requestURL: url, delay: 1)
        networking.reponseMocks.append(response)

        let expectations = expectationWithDescription("expectations")

        // Test
        contactsService.fetchContacts { (result) in

            // Verify
            switch result {
            case .Success(_):
                XCTFail("test should fail with error")

            case .Failure(let error):
                XCTAssertNotNil(error, "failed on error")
            }

            expectations.fulfill()
        }

        waitForExpectationsWithTimeout(10) { (error: NSError?) -> Void in
            guard error == nil else {
                XCTFail("The request took too long to be processed")
                return
            }
        }
    }
}
