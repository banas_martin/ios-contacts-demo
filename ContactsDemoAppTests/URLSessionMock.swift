//
//  URLSessionMock.swift
//  ContactsDemoApp
//
//  Created by Banas Martin on 14/08/16.
//  Copyright © 2016 Banas. All rights reserved.
//

import Foundation
@testable import ContactsDemoApp

class URLSessionMock: URLSessionProtocol {

    var reponseMocks = [TaskResponseMock]()

    func dataTaskWithRequest(request: NSURLRequest, completionHandler: DataTaskResult) -> URLSessionDataTaskProtocol {
        processDataTask(request, completionHandler: completionHandler)
        return URLSessionDataTaskMock()
    }

    private func processDataTask(request: NSURLRequest, completionHandler: DataTaskResult) {
        let responses = reponseMocks.filter {
            m in m.requestURL == request.URL!
        }

        if let r = responses.first {
            if r.delay > 0 {
                let dispatchSeconds: Double = r.delay
                let dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(dispatchSeconds * Double(NSEC_PER_SEC)))

                dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                    completionHandler(r.data, r.response, r.error)
                })

            } else {
                completionHandler(r.data, r.response, r.error)
            }
        }
    }
}